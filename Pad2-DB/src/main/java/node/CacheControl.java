package node;

import javafx.util.Pair;
import org.bson.Document;

import java.util.List;

class CacheControl {
    private static List<Document> cachedValue;
    private static boolean isActual = false;

    static Pair<List<Document>,String> readFromCache() {
        String cacheString = "cache";
        if (!isIsActual()) {
            writeToCache(LoadBalancer.getMusic());
            cacheString = String.valueOf(LoadBalancer.getNodeNr());
        }
        return new Pair<>(cachedValue, cacheString);
    }

    private static void writeToCache(List<Document> cachedValue) {
        CacheControl.cachedValue = cachedValue;
        setIsActual(true);
    }

    private static boolean isIsActual() {
        return isActual;
    }

    private static void setIsActual(boolean isActual) {
        CacheControl.isActual = isActual;
    }

    static void add(String author, String name) {
        setIsActual(false);
        LoadBalancer.add(author, name);
    }

    static void clearCollections() {
        setIsActual(false);
        LoadBalancer.clearCollections();
    }
}
