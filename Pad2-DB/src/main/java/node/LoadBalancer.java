package node;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class LoadBalancer {
    private static int LOAD=0;
    private static int nodeNr=1;

    public static int getNodeNr() {
        return nodeNr;
    }

    private static MongoDatabase db1 = new MongoHelper().getDataBase(MongoHelper.dataBaseServer1, "pad-2");
    private static MongoDatabase db2 = new MongoHelper().getDataBase(MongoHelper.dataBaseServer2, "pad-2-second");
    public static List<Document> getMusic(){
        MongoDatabase mongoDatabase=null;
        switch (LOAD) {
            case 0:
                mongoDatabase = db1;
                nodeNr=1;
                break;
            case 1:
                mongoDatabase = db2;
                nodeNr=2;
                break;
        }
        LOAD=(LOAD+1)%2;
        MongoCollection<Document> mongoCollection = mongoDatabase.getCollection("music");
        FindIterable<Document> findIterable = mongoCollection.find();
        List<Document> result = new ArrayList<>();
        findIterable.forEach((Consumer<? super Document>) result::add);
        return result;
    }

    public static void add(String author, String name) {
        Document doc = new Document("author", author).append("name",name);
        db1.getCollection("music").insertOne(doc);
        db2.getCollection("music").insertOne(doc);
    }

    public static void clearCollections(){
        db1.getCollection("music").drop();
        db2.getCollection("music").drop();
    }
}
