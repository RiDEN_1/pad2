package node;

import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

import java.util.Arrays;


public class MongoHelper {
   static public String dataBaseServer1 = "ds113443.mlab.com:13443";  //pad-2
   static public String dataBaseServer2 = "ds211774.mlab.com:11774";  //pad-2-second

   private static String name = "admin";
   private static String password = "pad2admin";
    public MongoDatabase getDataBase(String dataBaseServer, String dataBase) {
        MongoCredential credential = MongoCredential.createCredential(name, dataBase, password.toCharArray());
        MongoClient mongoClient = new MongoClient(new ServerAddress(dataBaseServer), Arrays.asList(credential));
        return mongoClient.getDatabase(dataBase);
    }

}
