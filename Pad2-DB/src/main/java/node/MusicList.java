package node;

import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class MusicList {
    private List<Music> musicList = new ArrayList<>();

    static class Music {
        Music(String author, String name) {
            this.author = author;
            this.name = name;
        }

        String author;
        String name;
    }

    public void setList(List<Document> docList) {
        docList.forEach(document -> musicList.add(
                new Music(document.getString("author"), document.getString("name"))));
    }

//    public void add(String author, String name) {
//        musicList.add(new Music(author, name));
//    }

    public String toHtmlList() {
        StringBuilder sb = new StringBuilder();
        for (Music listElement : musicList) {
            sb.append(listElement.author).append(" - ").append(listElement.name).append("<br>");
        }
        return new String(sb);
    }
}
