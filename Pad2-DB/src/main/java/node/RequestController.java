package node;

import javafx.util.Pair;
import org.bson.Document;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class RequestController {
    @RequestMapping("/getMusicList")
    public String main() {
        MusicList musicList = new MusicList();
        Pair<List<Document>,String> read = CacheControl.readFromCache();
        String message1 = "Received via node "+read.getValue()+"<br>Music list:<br>";
        musicList.setList(read.getKey());
        String message2="<br><a href=\"https://localhost:1488/add\">Add a track</a>";

        return message1 + musicList.toHtmlList() + message2;
    }

    @RequestMapping(value = "/add", params = {"author", "name"})
    public String addProcess(@RequestParam(value = "author") String author, @RequestParam(value = "name") String name) {
        CacheControl.add(author, name);
        return "Success";
    }

    @RequestMapping(value = "/clear")
    public String clear() {
        CacheControl.clearCollections();
        return "Cleared";
    }
}