package proxy;

import com.jayway.restassured.response.Response;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.jayway.restassured.RestAssured.given;


@RestController
public class PageController {
    @RequestMapping("/main")
    public String main() {
        //Response responseFromNode = given().get("https://localhost:228/getMusicList")
        Response responseFromNode = given().get("http://localhost:228/getMusicList")
                .then().extract().response();
        return responseFromNode.asString();
    }

    @RequestMapping(value = "/add", params = {"author", "name"})
    public String addProcess(@RequestParam(value = "author") String author, @RequestParam(value = "name") String name) {
        Response responseFromNode = given().get("http://localhost:228/add?author="+author+"&name="+name)
                .then().extract().response();
        String response="";
        if (responseFromNode.asString().equals("Success")){
            response+= "success<br>";
        } else response+= "failed<br>";
        return response +
                "<a href=\"https://localhost:1488/main\">Go back to main page</a>";
    }

    @RequestMapping(value = "/add")
    public String add() {
        return "<form>\n" +
                "  Author:<br>\n" +
                "  <input type=\"text\" name=\"author\"><br>\n" +
                "  Name:<br>\n" +
                "  <input type=\"text\" name=\"name\">\n" +
                "<button type=\"submit\">Submit</button><br>" +
                "</form>";
    }
    @RequestMapping(value = "/clear")
    public String clear() {
        //Response response = given().get("https://localhost:228/clear").then().extract().response();
        Response response = given().get("http://localhost:228/clear").then().extract().response();
        if (response.asString().equals("Cleared")){
            return "success";
        }
        return "failed";
    }
}